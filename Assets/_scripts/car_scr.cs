﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class car_scr : MonoBehaviour
{

    // +++ public fields +++
    [Header("References")]
    public Transform centerOfMass;


    // +++ private fields +++

    private Rigidbody _rb;
    [Header("CAR Specs")]
    public float wheelRadius;


	// Use this for initialization
	void Start ()
	{

	    _rb = GetComponent<Rigidbody>();

	}
	
	// Update is called once per frame
	void Update () {

        _rb.centerOfMass = centerOfMass.localPosition;   

	}
}
