﻿using UnityEngine;
using System.Collections;

public class suspension_scr : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private Rigidbody _rb;
    [SerializeField] private car_scr _carScript;


    [Header("Suspension")]
    public float springForce;
    public float damperForce;
    public float springConstant;
    public float damperConstant;
    public float restLength;

    
    private float _previousLength;
    private float _currentLength;
    private float _springVelocity; 


	// Use this for initialization
	void Start ()
	{
	    
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
	    RaycastHit rayCastHit;

	    if (Physics.Raycast(transform.position, -transform.up, out rayCastHit, restLength + _carScript.wheelRadius))
	    {
	        _previousLength = _currentLength;
	        _currentLength = restLength - (rayCastHit.distance - _carScript.wheelRadius);
	        _springVelocity = (_currentLength - _previousLength)/Time.fixedDeltaTime;
	        springForce = springConstant*_currentLength;
	        damperForce = damperConstant*_springVelocity;

	        _rb.AddForceAtPosition(transform.up*(springForce + damperForce), transform.position);
	    }
	}
}
