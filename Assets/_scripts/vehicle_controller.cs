﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class vehicle_axis : System.Object
{

    [Header("REFERENCES")]

    // +++ physics +++
    public WheelCollider wheelLeftCollider;
    public WheelCollider wheelRightCollider;

    // +++ visuals +++
    public GameObject wheelLeftMesh;
    public GameObject wheelRightMesh;

    [Header("AXIS-SETTINGS")]
    public bool canApplyMotorTorque;            // is this axis used for appling torque
    public bool canSteer;                       // is this axis used for steering the vehicle

}

public class vehicle_controller : MonoBehaviour
{

    [Header("CAR-SETTINGS")]
    public float motorMaxTorque;                // abstract number, which specifies the max power of the motor
    public float steeringMaxAngle;              // limit of steering in degrees

    public vehicle_axis[] vehicleAxis;          // container for the exiting axis on the vehicle


    // +++ private fields +++private 
    private float motorCurrentTorque;
    private float steeringCurrentAngle;
    private float brakeTorque;

    /// <summary>
    /// Visualizes the wheel for a given vehicle axis
    /// </summary>
    /// <param name="axis">The vehicle axis the wheels should be drawn for.</param>
    private void visualizeWheelsOnAxis(vehicle_axis axis)
    {
        // visualize the wheels according to their corellated wheel colliders
        drawWheel(axis.wheelLeftCollider, axis.wheelLeftMesh);
        drawWheel(axis.wheelRightCollider, axis.wheelRightMesh);
    }

    /// <summary>
    /// Visualize a sigle wheel mesh by its corresponding wheel collider
    /// </summary>
    /// <param name="wheelCollider">The wheel collider, that determines the visual appearence of the wheel mesh.</param>
    /// <param name="wheelMesh">The reference to the gameobject, that contains the wheel mesh.</param>
    private void drawWheel(WheelCollider wheelCollider, GameObject wheelMesh)
    {
        Quaternion rotation;                // used for storing the wheel rotation temporally
        Vector3 position;                   // used for storeing the wheel position temporallly
        
        // extract the wheel collider position and rotation
        wheelCollider.GetWorldPose(out position, out rotation);

        // copy this to the wheel mesh
        wheelMesh.transform.position = position;
        wheelMesh.transform.rotation = rotation;
    }

    public void Update()
    {
        // calculates forces
        motorCurrentTorque = motorMaxTorque * Input.GetAxis("Vertical");
        steeringCurrentAngle = steeringMaxAngle * Input.GetAxis("Horizontal");
        brakeTorque = Mathf.Abs(Input.GetAxis("Jump"));

        // brake
        if (brakeTorque > 0.001)
        {
            brakeTorque = motorMaxTorque;
            motorCurrentTorque = 0;
        }
        else
            brakeTorque = 0;

        // apply forces
        foreach (var axis in vehicleAxis)
        {
            // apply steering
            if (axis.canSteer)
            {
                axis.wheelLeftCollider.steerAngle = steeringCurrentAngle;
                axis.wheelRightCollider.steerAngle = steeringCurrentAngle;
            }

            // apply motor torque
            if (axis.canApplyMotorTorque)
            {
                axis.wheelLeftCollider.motorTorque = motorCurrentTorque;
                axis.wheelRightCollider.motorTorque = motorCurrentTorque;
            }

            // appy brake
            axis.wheelLeftCollider.brakeTorque = brakeTorque;
            axis.wheelRightCollider.brakeTorque = brakeTorque;

            // visualize axis wheels
            visualizeWheelsOnAxis(axis);
        }
    }
}
